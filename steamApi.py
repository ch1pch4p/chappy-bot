from string import Template

import pandas as pd
import ast
import json
from bson.json_util import dumps
from bson import json_util

from pymongo import MongoClient

import requests
from flask import Flask, request
from flask_restful import Resource, Api, reqparse

app = Flask(__name__)
api = Api(app)
client = MongoClient('jagdpanzer', 27017)
steamResolveVanity = Template('http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=${KEY}&vanityurl=${NAME}')
steamGetOwnedGames = Template('http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=${KEY}&steamid=${STEAM_ID}&format=json&include_appinfo=true')
steamGetAllApps = 'http://api.steampowered.com/ISteamApps/GetAppList/v2'
steamGetAppInfo = Template('http://store.steampowered.com/api/appdetails?appids=${APP_ID}')

class User(Resource):
        def __init__(self):
            with open('secrets') as f:
                data = json.load(f)
                self.secret_key = data['steam_key']
        

        def get(self, vanity_name):
            db = client['chappy-bot']
            col = db.steamUsers

            entry = col.find_one({"steam_vanity" : vanity_name })

            if entry is None:
                print('vanity ' + vanity_name + ' not found. Getting from steam...')
                url = steamResolveVanity.substitute(KEY=self.secret_key, NAME=vanity_name)
            
                response = requests.get(url)
                result_id = response.json()
                result_id_response = result_id['response']['steamid']
        
                ownedGamesUrl = steamGetOwnedGames.substitute(KEY=self.secret_key, STEAM_ID=result_id_response)
                ownedGamesResponse = requests.get(ownedGamesUrl)
                ownedGames = ownedGamesResponse.json()

                entry = { "steam_vanity" : vanity_name, "steam_id" : result_id_response, "owned_games": ownedGames['response']}
                col.insert_one(entry)
            
            return json.loads(dumps(entry)), 200           

class Users(Resource):

        def get(self):
            db = client['chappy-bot']
            col = db.steamUsers
            users = col.find({},{"steam_vanity" : 1, "owned_games" : {"game_count" : 1}})
            return json.loads(dumps(users)), 200           

class CommonGames(Resource):

        def get(self):
            users = request.args["users"]
            usersArr = users.split(",")
            if usersArr is None or len(usersArr) < 2:
                return None, 400, 

            arr = []
            for i in usersArr:
                db = client['chappy-bot']
                col = db.steamUsers    #get the list of users from the db, or load them
                #entry = col.find_one({"steam_vanity" : i },{"steam_vanity" : 1, "owned_games" : {"game_count" : 1}})
                entry = col.find_one({"steam_vanity" : i },{"_id":0,"owned_games" : {"games" : {"appid": 1, "name": 1}}})
                arr.append(entry)

            #print(arr)

            #use if you want to return the _id, but fill need to modify the find one above
            #return json.loads(json_util.dumps(arr)), 200
            
            return arr, 200

#         # Python program to illustrate the intersection 
# # of two lists using set() method 
#         def intersection(lst1, lst2): 
#             return list(set(lst1) & set(lst2)) 

api.add_resource(Users, '/Users')  # add endpoints
api.add_resource(User, '/User/<string:vanity_name>')  # add endpoints
api.add_resource(CommonGames, '/CommonGames')  # add endpoints

if __name__ == '__main__':
        app.run(debug = True, host = "localhost", port = 5500)  # run our Flask app
        