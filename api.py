from flask import Flask
from flask_restful import Resource, Api, reqparse
import pandas as pd
import ast
from pymongo import MongoClient

app = Flask(__name__)
api = Api(app)
client = MongoClient('localhost', 27017)


class Joke(Resource):
        def get(self):
            db = client['chappy-bot']
            col = db.jokes
            items = col.find()
            return  str(col.find_one()), 200  # return data and 200 OK


api.add_resource(Joke, '/jokes')  # add endpoints

if __name__ == '__main__':
        app.run(host = "192.168.2.200", port = 5500)  # run our Flask app
        
